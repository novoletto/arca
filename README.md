  - Desafio Arca Solutions

  - Requisitos:
  * Laravel 5.6+
  * MySQL 5.6
  * Composer


  - Instalar o projeto:
  * Clonar o repositório

  - Criar o banco de dados
```javascript
php artisan migrate
```
  - Cadastrar Categorias
```javascript
php artisan db:seed --class=CategoriasSeeder
```
  - Cadastrar Empresas
```javascript
php artisan db:seed --class=RegisterBusiness
```

Crie uma conta no menu "Register" para poder ter acesso ao cadastro de Empresas
