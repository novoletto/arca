<?php

use Illuminate\Database\Seeder;
use App\Categoria;
use App\Business;

class RegisterBusiness extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      for ($i=0; $i < 15; $i++) {
        $business = new Business;

        $business->titulo = "Empresa " . $i;
        $business->telefone = rand(1111111111,2000000000);
        $business->cep = rand(11111111,99999999);
        $business->cidade = "Cidade " . $i;
        $business->estado = "Estado " . $i;
        $business->endereco = "Endereço " . $i;
        $business->descricao = "Descrição  " . $i;

        $business->save();

        $business->categorias()->attach($business->categorias);
      }
    }
}
