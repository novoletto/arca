<?php

use Illuminate\Database\Seeder;
use App\Categoria;

class CategoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $categorias = array('Auto', 'Beauty and Fitness', 'Entertainment', 'Food and Dining', 'Food and Dining', 'Health', 'Sports', 'Travel');

      foreach ($categorias as $key => $value) {
        Categoria::create(array('nome' => $value));
      }
    }
}
