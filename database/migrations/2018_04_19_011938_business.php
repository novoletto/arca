<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Business extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('business', function (Blueprint $table) {
          $table->increments('id');
          $table->string('titulo');
          $table->string('telefone');
          $table->string('endereco');
          $table->string('cep');
          $table->string('cidade');
          $table->string('estado');
          $table->string('descricao');
          $table->integer('categoria_id');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
