<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriaBusinessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categoria_business', function (Blueprint $table) {
          $table->integer('categoria_id')->unsigned()->nullable();
          $table->foreign('categoria_id')->references('id')
            ->on('categorias')->onDelete('cascade');

          $table->integer('business_id')->unsigned()->nullable();
          $table->foreign('business_id')->references('id')
            ->on('business')->onDelete('cascade');

          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categoria_business');
    }
}
