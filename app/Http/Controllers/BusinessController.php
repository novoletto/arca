<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BusinessRequest;
use App\Business;
use App\Categoria;
use DB;
use Auth;

class BusinessController extends Controller
{
  public function search(Request $request) {
    //return response()->json($request);
    $empresas = DB::table('business as b')
      ->join('business_categoria as cb', 'cb.business_id', '=', 'b.id')
      ->join('categorias as c', 'c.id', '=', 'cb.categoria_id')
      ->where('titulo', 'like', "%$request->business%")

      ->get();
      return view('empresa', array('empresas' => $empresas));
    return response()->json($empresas);
  }

  public function index() {
    return view('empresa', array('categorias' => $this->getCategorias()));
  }

  private function getCategorias() {
    $categorias = DB::table('categorias')->get();
    return $categorias;
  }

  public function cadastrar() {
    if (Auth::check()) {
      return view('cadastrar/empresa', array('categorias' => $this->getCategorias()));
    } else {
      return view('auth/login');
    }

  }

  public function create(BusinessRequest $request) {
    //return response()->json($request);
    $business = new Business($request->all());
    
    $business->save();

    $business->categorias()->attach($request->categorias);

    return view('/empresa', array('empresas' => $this->getEmpresas()));
  }

  private function getEmpresas() {
    $empresas = DB::table('business')->get();
    return $empresas;
  }

  private function getEmpresa($id) {
    $empresa = DB::table('business')->first();
    return $empresa;
  }

  public function ver_empresa($id) {
    return view('ver_empresa', array('empresa' => $this->getEmpresa($id)));
  }
}
