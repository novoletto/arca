<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    protected $table = "business";

    protected $fillable = [
        'titulo', 'telefone', 'endereco', 'cep', 'cidade', 'estado', 'descricao'
    ];

    public function categorias()
    {
      return $this->belongsToMany('App\Categoria')
        ->withTimestamps();
      }
}
