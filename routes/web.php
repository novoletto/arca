<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'BusinessController@cadastrar')->name('home');

Route::post('/buscar', 'BusinessController@search')->name('buscar');

Route::get('/empresa', 'BusinessController@index')->name('empresa');

Route::get('/cadastrar/empresa', 'BusinessController@cadastrar')->name('cadastrarempresa');

Route::post('/cadastrar/empresa', 'BusinessController@create')->name("create_business");

Route::get('/empresa/{id}', 'BusinessController@ver_empresa')->name("ver_empresa");
