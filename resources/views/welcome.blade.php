@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
          <form method="POST" action="{{ route('buscar') }}">
          @csrf
            <div class="form-group row">
              <div class="col-md-10">
                  <input id="business" type="text" class="form-control{{ $errors->has('business') ? ' is-invalid' : '' }}" name="business" value="{{ old('business') }}" required autofocus placeholder="Localizar Empresa...">

                  @if ($errors->has('business'))
                      <span class="invalid-feedback">
                          <strong>{{ $errors->first('business') }}</strong>
                      </span>
                  @endif
              </div>
              <div class="col-md-2">
                <button class="btn btn-primary" type="submit">Localizar</button>
              </div>
            </div>
          </form>
      </div>
    </div>
</div>
@stop
