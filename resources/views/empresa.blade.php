@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12 text-center">
        <a href="/"><h3>Voltar</h3></a>
      </div>
      <div class="col-md-12">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Nome</th>
            <th>Endereço</th>
            <th>Telefone</th>
            <th>CEP</th>
          </tr>
        </thead>
          <tbody>
            @if (isset($empresas) && count($empresas))
              @foreach ($empresas as $empresa)
              <tr class='clickable-row' data-href='/empresa/{{$empresa->id}}'>
                <td>{{$empresa->titulo}}</td>
                <td>{{$empresa->endereco}}</td>
                <td>{{$empresa->telefone}}</td>
                <td>{{$empresa->cep}}</td>
              </tr>
              @endforeach
            @else
            <td colspan="4" class="text-center">Nenhum resultado encontrado.</td>
            @endif
          </tbody>
        </table>
      </div>
    </div>
</div>
<script
  src="http://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
<script>
jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});
</script>
@stop
