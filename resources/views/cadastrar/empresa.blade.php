@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
          <form method="POST" action="{{ route('create_business') }}">
          @csrf
            <div class="form-group row">
              <div class="col-md-6 form-group">
                  <input id="titulo" type="text" class="form-control{{ $errors->has('titulo') ? ' is-invalid' : '' }}" name="titulo" value="{{ old('titulo') }}" required autofocus placeholder="Título">

                  @if ($errors->has('titulo'))
                      <span class="invalid-feedback">
                          <strong>{{ $errors->first('titulo') }}</strong>
                      </span>
                  @endif
              </div>
              <div class="col-md-6 form-group">
                  <input id="telefone" type="text" class="form-control{{ $errors->has('telefone') ? ' is-invalid' : '' }}" name="telefone" value="{{ old('telefone') }}" required autofocus placeholder="Telefone">

                  @if ($errors->has('telefone'))
                      <span class="invalid-feedback">
                          <strong>{{ $errors->first('telefone') }}</strong>
                      </span>
                  @endif
              </div>
              <div class="col-md-6 form-group">
                  <input id="endereco" type="text" class="form-control{{ $errors->has('endereco') ? ' is-invalid' : '' }}" name="endereco" value="{{ old('endereco') }}" required autofocus placeholder="Endereço">

                  @if ($errors->has('endereco'))
                      <span class="invalid-feedback">
                          <strong>{{ $errors->first('endereco') }}</strong>
                      </span>
                  @endif
              </div>
              <div class="col-md-6 form-group">
                  <input id="cep" type="text" class="form-control{{ $errors->has('cep') ? ' is-invalid' : '' }}" name="cep" value="{{ old('cep') }}" required autofocus placeholder="CEP">

                  @if ($errors->has('cep'))
                      <span class="invalid-feedback">
                          <strong>{{ $errors->first('cep') }}</strong>
                      </span>
                  @endif
              </div>
              <div class="col-md-6 form-group">
                  <input id="cidade" type="text" class="form-control{{ $errors->has('cidade') ? ' is-invalid' : '' }}" name="cidade" value="{{ old('cidade') }}" required autofocus placeholder="Cidade">

                  @if ($errors->has('cidade'))
                      <span class="invalid-feedback">
                          <strong>{{ $errors->first('cidade') }}</strong>
                      </span>
                  @endif
              </div>
              <div class="col-md-6 form-group">
                  <input id="estado" type="text" class="form-control{{ $errors->has('estado') ? ' is-invalid' : '' }}" name="estado" value="{{ old('estado') }}" required autofocus placeholder="Estado">

                  @if ($errors->has('estado'))
                      <span class="invalid-feedback">
                          <strong>{{ $errors->first('estado') }}</strong>
                      </span>
                  @endif
              </div>
              <div class="col-md-6 form-group">
                  <input id="descricao" type="text" class="form-control{{ $errors->has('descricao') ? ' is-invalid' : '' }}" name="descricao" value="{{ old('descricao') }}" required autofocus placeholder="Descrição">

                  @if ($errors->has('descricao'))
                      <span class="invalid-feedback">
                          <strong>{{ $errors->first('descricao') }}</strong>
                      </span>
                  @endif
              </div>
              <div class="col-md-12 form-group">
                <label for="categorias">Selecione as categorias</label>
                <select multiple class="form-control{{ $errors->has('categorias') ? ' is-invalid' : '' }}" id="categorias" name="categorias[]">
                  @foreach ($categorias as $categoria)
                    <option value="{{$categoria->id}}">{{$categoria->nome}}</option>
                  @endforeach
                </select>
                @if ($errors->has('categorias'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('categorias') }}</strong>
                    </span>
                @endif
              </div>
              <div class="col-md-2">
                <button class="btn btn-primary" type="submit">Cadastrar</button>
              </div>
            </div>
          </form>
      </div>
    </div>
</div>
@stop
