@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
          @csrf
            <div class="form-group row">
              <div class="col-md-6 form-group">
                <label>Nome</label>
                  <input id="titulo" type="text" class="form-control" disabled name="titulo" value="{{ $empresa->titulo }}" autofocus placeholder="Título">
              </div>
              <div class="col-md-6 form-group">
                <label>Telefone</label>
                  <input id="telefone" type="text" class="form-control" disabled name="telefone" value="{{ $empresa->telefone }}" autofocus placeholder="Telefone">
              </div>
              <div class="col-md-6 form-group">
                <label>Endereço</label>
                  <input id="endereco" type="text" class="form-control" disabled name="endereco" value="{{ $empresa->endereco }}" autofocus placeholder="Endereço">
              </div>
              <div class="col-md-6 form-group">
                <label>CEP</label>
                  <input id="cep" type="text" class="form-control" disabled name="cep" value="{{ $empresa->cep }}" autofocus placeholder="CEP">
              </div>
              <div class="col-md-6 form-group">
                <label>Cidade</label>
                  <input id="cidade" type="text" class="form-control" disabled name="cidade" value="{{ $empresa->cidade }}" autofocus placeholder="Cidade">
              </div>
              <div class="col-md-6 form-group">
                <label>Estado</label>
                  <input id="estado" type="text" class="form-control" disabled name="estado" value="{{ $empresa->estado }}" autofocus placeholder="Estado">
              </div>
              <div class="col-md-6 form-group">
                <label>Descricao</label>
                  <input id="descricao" type="text" class="form-control" disabled name="descricao" value="{{ $empresa->descricao }}" autofocus placeholder="Descrição">
              </div>
            </div>
      </div>
    </div>
</div>
@stop
